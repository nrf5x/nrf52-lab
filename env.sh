TOP_DIR=`pwd`

#https://www.nordicsemi.com/-/media/Software-and-other-downloads/SDKs/nRF5/Binaries/nRF5SDK15209412b96.zip
NRF_SDK="nRF5_SDK_15.2.0_9412b96"
NRF_CMD="nRF-Command-Line-Tools_9_8_1_Linux-$(arch)"

mkdir -p tools
cd tools

if [ ! -d "${NRF_SDK}" ]; then
    echo "Please Download ${NRF_SDK}"
else 
    export SDK_ROOT=${PWD}/${NRF_SDK}
    echo "nRF SDK OK!"
fi


if [ ! -d "${NRF_CMD}" ]; then
    echo "Please Download ${NRF_CMD}"
else 
    export PATH=$PWD/${NRF_CMD}/nrfjprog:$PATH 
    export PATH=$PWD/${NRF_CMD}/mergehex:$PATH 

    echo "nRF CMD OK!"
fi


cd ${TOP_DIR}
